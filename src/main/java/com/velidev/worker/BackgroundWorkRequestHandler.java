package com.velidev.worker;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.*;

/**
 * Created by Nikola on 7/18/2015.
 */
public class BackgroundWorkRequestHandler<T, R> {

    private final List<WorkerRunnable<T, R>> mQueue = new ArrayList<>();
    private final ThreadPoolExecutor mExecutorPool;

    public BackgroundWorkRequestHandler() {
        ThreadFactory threadFactory = Executors.defaultThreadFactory();
        mExecutorPool = new ThreadPoolExecutor(2, 4, 10, TimeUnit.SECONDS,
                new ArrayBlockingQueue<Runnable>(2), threadFactory,
                new RejectedExecutionHandler() {

                    @Override
                    public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
                        ((WorkerRunnable) r).inProcess = false;
                    }
                });
    }

    /**
     * Makes a request to working thread. If there is a request in process it will be canceled and new one will be scheduled instead.
     * This check is performed based on a value provided by BackgroundWorkRequestListener.getId method.
     *
     * @param worker          real work is performed in this method
     * @param requestListener listener which will be notified once work is done
     * @param input           what should be processed in background
     */
    public void request(Worker<T, R> worker,
                        BackgroundWorkRequestListener<T, R> requestListener, T input, Object payload) {
        // 1. check da nije u queue vec
        // 2. ako jeste stari cancel, novi add na kraj
        // 3. prosledimo executoru ako odbije, onda ga dodao u mQueue
        // 4. threadovi procesuiraju
        // 5. obavestimo listenere i cao

        // searching if the request for this id is already in queue
        synchronized (mQueue) {
            Iterator<WorkerRunnable<T, R>> iterator = mQueue.iterator();
            while (iterator.hasNext()) {
                WorkerRunnable<T, R> runnable = iterator.next();
                BackgroundWorkRequestListener<T, R> listener = runnable.listener;
                // this is it -> same id
                if (runnable.requesterId == requestListener.getId()) {
                    if (input.equals(runnable.input)) {
                        // this is same id, same path = same request
                        return;
                    } else {
                        // we have in queue request for this id but different
                        // path, lets just cancel old one
                        iterator.remove();
                        runnable.obsolete = true;
                        cancelProcessing(runnable);
                        break;
                    }
                }
            }

            // adding request to the queue
            WorkerRunnable<T, R> runnable = new WorkerRunnable<>();
            runnable.handler = this;
            runnable.input = input;
            runnable.worker = worker;
            runnable.requesterId = requestListener.getId();
            runnable.listener =
                    requestListener;
            runnable.payload = payload;
            mQueue.add(runnable);
            executeNext();
        }
    }

    private void cancelProcessing(WorkerRunnable runnable) {
        if (runnable != null) {
            mExecutorPool.remove(runnable);
            if (runnable instanceof Future<?> && ((Future<?>) runnable).isCancelled()) {
                Future<?> future = (Future<?>) runnable;
                future.cancel(true);
            }
            mExecutorPool.purge();
        }
    }

    void onThreadFinished(final WorkerRunnable<T, R> runnable, final R output,
                          final BackgroundWorkRequestListener<T, R> listener) {
        synchronized (mQueue) {
            runnable.inProcess = false;
            mQueue.remove(runnable);
            executeNext();
            listener.onResult(runnable.input, output, runnable.payload);
        }
    }

    private void executeNext() {
        synchronized (mQueue) {
            Iterator<WorkerRunnable<T, R>> iterator = mQueue.iterator();
            while (iterator.hasNext()) {
                WorkerRunnable<T, R> runnable = iterator.next();
                if (!runnable.inProcess) {
                    runnable.inProcess = true;
                    mExecutorPool.execute(runnable);
                }
            }
        }
    }

    void removeThread(WorkerRunnable<T, R> runnable) {
        synchronized (mQueue) {
            mQueue.remove(runnable);
        }
    }

    public void onDestroy() {
        // shut down the pool
        mExecutorPool.shutdown();
        mQueue.clear();
    }

}
