package com.velidev.worker;

//TODO write javadocs
public interface BackgroundWorkRequestListener<T, R> {

    /**
     * This will be called once processing is done.
     *
     * @param input  what to process
     * @param output processed result
     */
      void onResult(T input, R output, Object payload);

      int getId();

}
