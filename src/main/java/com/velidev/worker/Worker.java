package com.velidev.worker;

/*
 * TODO write java docs
 */
public interface Worker<T, R> {

      R doWork(T input, Object payload);

}
