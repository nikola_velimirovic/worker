package com.velidev.worker;

/**
 * TODO write javadocs
 */
class WorkerRunnable<T, R> implements Runnable {

    BackgroundWorkRequestHandler<T, R> handler;
    T input;
    int requesterId;
    boolean inProcess = false;
    Worker<T, R> worker;
    public BackgroundWorkRequestListener<T, R> listener;
    boolean obsolete = false;
    public Object payload;

    public WorkerRunnable() {

    }

    public void run() {
        if (!obsolete) {
            inProcess = true;
            R output = worker.doWork(input, payload);
            inProcess = false;
            if (listener != null && !obsolete) {
                handler.onThreadFinished(this, output, listener);
            } else {
                handler.removeThread(this);
            }
        }
    }
}
