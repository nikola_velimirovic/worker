package com.velidev.worker;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Nikola on 7/21/2015.
 */
public class BackgroundWorkRequestHandlerTest {

    private BackgroundWorkRequestHandler<Integer, Integer> mRequestHandler;
    private Worker<Integer, Integer> mWorker;
    private int mRequestedCount = 0;
    private int mFinishedCount = 0;

    @Before
    public void onBuildUp() {
        mRequestHandler = new BackgroundWorkRequestHandler<>();
        mWorker = new Worker<Integer, Integer>() {

            public Integer doWork(Integer input, Object payload) {
                try {
                    Thread.sleep(20);
                } catch (InterruptedException e) {
                    System.err.println("error at " + e);
                }
                mFinishedCount++;
                return input + 1;
            }
        };
    }


    @Test
    public void testQueueing() {
        for (int i = 0; i < 300; i++) {
            final int id = i % 3;
            mRequestedCount++;
            mRequestHandler.request(mWorker, new BackgroundWorkRequestListener<Integer, Integer>() {

                @Override
                public void onResult(Integer input, Integer output, Object payload) {
                    assertEquals(input + 1, (int) output);
                }

                public int getId() {
                    return id;
                }

            }, i, i);

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
//        assertEquals(mRequestedCount, mFinishedCount);
        System.out.println("mRequestedCount " + mRequestedCount + " mFinishedCount " + mFinishedCount);
    }

    @After
    public void onDestroy() {
        mRequestHandler.onDestroy();
        mWorker = null;
        mRequestHandler = null;
    }

}
